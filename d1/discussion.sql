[SECTION] Inserting Records

--inserting data to a particular table and 1 column
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");


--inserting data to a particular table and/with 2 or more column
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs  (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 3);


INSERT INTO songs  (song_name, length, genre, album_id) VALUES ("214", 300, "OPM", 4);
INSERT INTO songs  (song_name, length, genre, album_id) VALUES ("Ulan", 340, "OPM", 4);


[SECTION] --READ AND SELECT RECORDS/DATA

--Display the title and genre of all the songs

SELECT song_name, genre FROM songs; 

--Display the song name of all the OPM songs.

SELECT song_name FROM songs WHERE genre = "OPM";

--Display the title of all the songs. 
SELECT * FROM songs;

--Display the title and length of the OPM songs that are more than 3 minutes

SELECT song_name, length FROM songs WHERE length > 301 AND genre = "OPM";

[SECTION] Updating Records

update songs SET length = 259 where song_name = "214";


update songs SET song_name = "Kundiman Updated" where song_name = "Ulan";


[SECTION] --Delete records

DELETE FROM songs WHERE genre = "OPM" AND length > 300;

--Delete all from a table

DELETE * FROM songs WHERE genre = "K-pop";